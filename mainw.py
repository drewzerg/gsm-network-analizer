# coding: cp1251
# ���������� ��� ����������� �������
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from lib_tft24T import TFT24T
import spidev
# ���������� ��� ������ � SIM800
import serial
import urllib2
import re
# ���������� ��� ������ � GPIO
import RPi.GPIO as GPIO
from time import sleep
import time
import threading
import Queue
import csv

q = Queue.Queue()
# ������������ Raspberry Pi
# ��������� GPIO ����� ��� ������
LEFT = 27
RIGHT = 17
UP = 22
DOWN = 4
buttons = [LEFT, RIGHT, UP, DOWN]
GPIO.setmode(GPIO.BCM)
GPIO.setup(buttons, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
RED = 13
GREEN = 12
BLUE = 6
RGB_led = [RED, GREEN, BLUE]
GPIO.setup(RGB_led, GPIO.OUT)
GPIO.output(RGB_led, GPIO.LOW)
GPIO.setwarnings(False)
# ��������� LCD �������
DC = 24
RST = 25
LED = 5
# ������������� �������
TFT = TFT24T(spidev.SpiDev(), GPIO, landscape=False)
TFT.initLCD(DC, RST, LED)
# Draw the image on the display hardware.
H1 = ImageFont.truetype('FreeSans.ttf', 40)
H2 = ImageFont.truetype('FreeSans.ttf', 14)
H3 = ImageFont.truetype('FreeSans.ttf', 12)
# ��������� �����
BIG_MAP = [(29.65862, 30.55435), (59.78129, 60.12095)]

MMAP_URL = 'http://www.google.com/glm/mmap'
GOOGLE_MAPS_URL = 'http://maps.google.com'


class GSM_module(object):

    def __init__(self, baud):
        """Constructor"""
        BAUDRATE = baud
        self.port = serial.Serial(port='/dev/ttyAMA0',
                                  baudrate=BAUDRATE,
                                  bytesize=8,
                                  parity='N',
                                  stopbits=1,
                                  timeout=1,
                                  )
        pass

    def echo(self, text):
        print(text)

    def read(self):
        data = self.port.read(1)
        n = self.port.inWaiting()
        while n:
            data += self.port.read(n)
            n = self.port.inWaiting()

        print("~echo~:\n{}".format(data))
        return data

    def write(self, command):
        command += '\r\n'
        self.port.write(command)

    def handshake(self):
        flag = False
        self.echo('handshaking...')
        self.write('AT')
        if self.read().find('OK') != -1:
            self.echo('handshaking well')
            flag = True
        else:
            self.echo('handshaking bad')
            flag = False

        self.write('ATE0')
        self.read()

        self.write('AT+CPAS')
        if self.read().find('+CPAS: 0') != -1:
            self.echo('module ready')
        else:
            self.echo("module is not ready")
        self.write('AT+CREG?')
        if self.read().find(',1') != -1:
            self.echo('Got connection')
        else:
            self.echo('No connection. Chek network')
        return flag

    def setup(self):
        self.write("AT+SAPBR=0,1")
        self.read()
        self.write('''AT+SAPBR=3,1,"CONTYPE","GPRS"''')
        self.read()
        self.write('''AT+SAPBR=3,1,"APN","internet.megafon.ru"''')
        self.read()
        self.write('''AT+SAPBR=3,1,"USER","megafon"''')
        self.read()
        self.write('''AT+SAPBR=3,1,"PWD","megafon"''')
        self.read()
        self.write("AT+SAPBR=1,1")
        self.read()
        self.write("AT+SAPBR=2,1")
        if self.read().find('+SAPBR: 1,1,"100.') != -1:
            return True
        else:
            return False

    def firststart(self):
        for i in range(5):
            flag_handshake = False
            flag_setup = False
            if SIM800.handshake():
                SIM800.echo('Handshake complite. Starting setup...')
                flag_handshake = True
            else:
                flag_handshake = False
            if SIM800.setup():
                SIM800.echo('Setup complite!')
                flag_setup = True
            else:
                SIM800.echo('Something went wrong. No simcard?')
                flag_setup = False
            if flag_handshake and flag_setup:
                SIM800.echo('All systems ready')
                self.read()
                return 1
            elif flag_handshake and not flag_setup:
                if i == 2:
                    return 2
                self.read()
                SIM800.echo('GPRS in not ready')
                SIM800.echo('Trying once again')
                time.sleep(5)

            else:
                SIM800.echo('Trying once again')
                time.sleep(7)

    def get_signal_level(self):
        SIM800.echo('Gettin signal level...')
        module_min = 2
        module_max = 30
        dbm_min = -110
        dbm_max = -54
        module_value_span = module_max - module_min
        actual_dbm_span = dbm_max - dbm_min
        self.write('AT+CSQ')
        reply = self.read()
        find_level = re.findall("\d+\,\d+", reply)
        if find_level != []:
            reply = find_level[0]
            reply = reply.replace(',', '.')
            module_csq_output = float(reply)
            valueScaled = float(module_csq_output - module_min) / float(module_value_span)
            signal_level = dbm_min + (valueScaled * actual_dbm_span)
            return signal_level
        else:
            pass

    def get_available_operators(self):
        operators_found = False
        Operators = []
        get_operators = 'AT+COPS=?' + '\r\n'
        self.port.write(get_operators)
        response = self.port.readline()
        while not operators_found:
            response = self.port.readline()
            if (str(response).startswith("+COPS")):
                operators_found = True
        response = response.replace('"', '')
        response = response.replace('),', '')
        response = response.split('(')
        del response[0]
        del response[-2]
        del response[-1]
        for element in range(len(response)):
            response[element] = response[element].replace('MOTIV', 'TELE2')
            response[element] = response[element].split(',')
            Operators.append(response[element][2])
        SIM800.read()
        return Operators

    def get_location(self):
        self.port.write("AT+CIMI\r\n")
        print(self.port.read(32))
        self.port.write("AT+CREG=1\r\n")
        print(self.port.read(64))
        self.port.write("AT+CREG?\r\n")
        print(self.port.read(64))
        self.port.write("AT+CREG?\r\n")
        print(self.port.read(64))

    def get_gsm_location(self):
        t = time.time()
        command = "AT+CIPGSMLOC=1,1\r\n"
        self.port.write(command)
        raw_response = ''
        while raw_response.find('+CIPGSM') == -1:
            raw_response += self.port.read(1)
        raw_response = self.read()
        find_location = re.findall("\d+\.\d+", raw_response)
        location = []
        location.append(float(find_location[1]))
        location.append(float(find_location[0]))
        print(time.time() - t)
        return location

    def find_cells_around(self, input):
        list = input
        locations = []
        lat_mid = 0.0
        lon_mid = 0.0
        counter = 0
        for cell in list:
            cell_mnc = int(cell['MNC'])
            cell_LAC = int(cell['LAC'], 16)
            cell_CELLID = int(cell['CELLID'], 16)
            if cell_mnc == 1:
                cell_scv = 'saint_petersburg_cell_towers_mts_gsm.csv'
            elif cell_mnc == 2:
                cell_scv = 'saint_petersburg_cell_towers_megafon_gsm.csv'
            elif cell_mnc == 99:
                cell_scv = 'saint_petersburg_cell_towers_beeline_gsm.csv'
            elif cell_mnc == 20:
                cell_scv = 'saint_petersburg_cell_towers_tele2_gsm.csv'
            with open(cell_scv) as csv_file:
                csv_reader = csv.reader(csv_file)
                for row in csv_reader:
                    if row != []:
                        radio, mcc, mnc, lac, cellid, lon, lat, bs_range, samples = row
                        if mnc == str(cell_mnc) and lac == str(cell_LAC) and cellid == str(cell_CELLID):
                            latt = float(lat)
                            long = float(lon)
                            cell_range = int(bs_range)
                            if cell_range < 1001:
                                counter += 1
                                lat_mid += latt
                                lon_mid += long
                            location = [latt, long, cell_range]
                            print(location)
                            if cell['Operator'] == 'MOTIV':
                                cell['Operator'] = '  TELE2  '
                            elif cell['Operator'] == 'BeeLineGSM':
                                cell['Operator'] = '  Beeline  '
                            elif cell['Operator'] == 'MTS':
                                cell['Operator'] = '    MTS    '
                            elif cell['Operator'] == 'MegaFon':
                                cell['Operator'] = ' Megafon '
                            location.append(cell['Operator'])
                            location.append(int(cell['Rxlevel']))
                            location.append(cell['Arfcn'])
                            locations.append(location)
                            break
        print('Hey, look like, you are here now: {latt}, {long}'.format(latt=lat_mid / counter, long=lon_mid / counter))
        point = [lat_mid / counter, lon_mid / counter]
        return [locations, point]

    def get_location_of_cells(self, input):
        output_list = input
        locations = []
        for cell in output_list:
            mcc = int(cell['MCC'], 16)
            mnc = int(cell['MNC'], 16)
            mcc_mnc = int(cell['MCC'] + cell['MNC'], 16)
            LAC = int(cell['LAC'], 16)
            CELLID = int(cell['CELLID'], 16)
            location = self.get_location_by_cell(mcc_mnc, LAC, CELLID)
            if location[1] > 100.0:
                location[1] = 228

            else:
                if cell['Operator'] == 'MOTIV':
                    cell['Operator'] = '  TELE2  '
                elif cell['Operator'] == 'BeeLineGSM':
                    cell['Operator'] = '  Beeline  '
                elif cell['Operator'] == 'MTS':
                    cell['Operator'] = '    MTS    '
                elif cell['Operator'] == 'MegaFon':
                    cell['Operator'] = ' Megafon '
                location.append(cell['Operator'])
                location.append(cell['Rxlevel'])
                location.append(cell['Arfcn'])
                locations.append(location)
        return locations

    def get_location_by_cell(self, mcc_mnc_dec, lac_dec, cid_dec):
        MMAP_URL = 'http://www.google.com/glm/mmap'
        # Use Google to get coordinates
        a = '000E00000000000000000000000000001B0000000000000000000000030000'
        h1, h2 = divmod(mcc_mnc_dec, 100)  # 152833 >>> (1528, 33)
        b = hex(cid_dec)[2:].zfill(8) + hex(lac_dec)[2:].zfill(8)
        # b >>> '0000e76a00008174'
        c = hex(h1)[2:].zfill(8) + hex(h2)[2:].zfill(8)
        # c >>> '000005f800000021'

        data = (a + b + c + 'FFFFFFFF00000000').decode('hex')
        response = urllib2.urlopen(MMAP_URL, data)
        res_hex = response.read().encode('hex')
        # res_hex >>> '000e1b0000000002e5bfbd020f49a50000097b0000004b0000'

        latitude = float(int(res_hex[14:22], 16)) / 1000000  # 48.611261
        longitude = float(int(res_hex[22:30], 16)) / 1000000  # 34.556325
        radius = float(int(res_hex[30:38], 16)) / 1

        return [latitude, longitude, radius]

    def get_bs_around(self):
        SIM800.read()
        Keys = ['Operator', 'MCC', 'MNC', 'Rxlevel', 'CELLID', 'Arfcn', 'LAC']
        OUTPUT_LIST = []
        command_1 = 'AT+CNETSCAN=1'
        command_2 = 'AT+CNETSCAN'
        self.write(command_1)
        self.read()
        self.write(command_2)
        raw_response = ''
        while raw_response.find('OK') == -1:
            raw_response += self.port.read(1)
        raw_response = raw_response.replace('MCC:', '')
        raw_response = raw_response.replace('MNC:', '')
        raw_response = raw_response.replace('Rxlev:', '')
        raw_response = raw_response.replace('Cellid:', '')
        raw_response = raw_response.replace('Arfcn:', '')
        raw_response = raw_response.replace('Lac:', '')
        raw_response = raw_response.replace('OK', '')
        raw_response = raw_response.replace('"', '')
        raw_response = raw_response.replace(' ', '')
        raw_response = raw_response.replace('\r\n', '')
        basestations_information = raw_response.split('Operator:')
        del basestations_information[0]
        for i in range(len(basestations_information)):
            basestations_information[i] = ',' + basestations_information[i]
            basestations_information[i] = basestations_information[i].split(',')
            del basestations_information[i][0]
        for bs in basestations_information:
            OUTPUT_LIST.append(dict(zip(Keys, bs)))
        print(OUTPUT_LIST)
        basestations_location = self.find_cells_around(OUTPUT_LIST)
        return basestations_location


def led(color, state):
    GPIO.output(color, state)


def map_choose(coord):
    coordinates = coord
    CENTER_old_city = [(30.1688, 30.40295), (59.89273, 59.98078)]
    CENTER_right_city = [(30.33257, 30.5608), (59.89273, 59.98078)]
    NORTH_west = [(30.12074, 30.34801), (59.97993, 60.06672)]
    NORTH_west_border = [(30.07336, 30.31746), (60.0417, 60.13056)]
    NORTH_west_sestroretsk_kronshtadt = [(29.65897, 30.10803), (59.95501, 60.12509)]
    NORTH_east = [(30.3075, 30.5365), (59.98027, 60.06638)]
    NORTH_east_border = [(30.28141, 30.52311), (60.04599, 60.13159)]
    SOUTH_west_petergof = [(29.65862, 30.11009), (59.94573, 59.78336)]
    SOUTH_west = [(30.107735, 30.33806), (59.79683, 59.88394)]
    SOUTH_east = [(30.30682, 30.53375), (59.80944, 59.89548)]

    # ����������� ������������ ����� �����
    CENTER_VERT = (BIG_MAP[0][0] + BIG_MAP[0][1]) / 2
    # ����������� �������������� ����� �����
    CENTER_HOR = (BIG_MAP[1][0] + BIG_MAP[1][1]) / 2
    # ������� ������ ������
    NORTH_BORDER = 60.06655
    ABOVE_CENTER = CENTER_old_city[1][1]
    CENTER_CITY_VERTICAL = 30.3648
    BELOW_CENTER = CENTER_old_city[1][0]
    map = []
    image = ''
    # ����� ������� �����
    if coordinates[1] < CENTER_VERT:  # ����� ����� ������� �����
        if coordinates[0] < CENTER_HOR:  # ������ ����� ������� �����
            print('PETERGOF')
            map = SOUTH_west_petergof
            image = 'petergof.jpg'
        else:  # ������� ����� ������� �����
            print('SESTRORETSK')
            map = NORTH_west_sestroretsk_kronshtadt
            image = 'sestroredsk_kronshdatd.jpg'

    else:  # ������ ����� ������� �����
        if coordinates[1] < CENTER_CITY_VERTICAL:  # ��������� ����� ������
            if coordinates[0] > ABOVE_CENTER:  # ������-�����
                if coordinates[0] > NORTH_BORDER:  # ������-�������� ������� ������
                    print("North west border")
                    map = NORTH_west_border
                    image = 'north_west_border'
                else:
                    print("North west")
                    map = NORTH_west
                    image = 'north_west.jpg'
            elif coordinates[0] < BELOW_CENTER:  # ��� �������� �����
                print("South west")
                map = SOUTH_west
                image = 'south_west.jpg'
            else:
                print("Old city")
                map = CENTER_old_city
                image = 'old_city.jpg'
        else:
            if coordinates[0] > ABOVE_CENTER:  # ������-������
                if coordinates[0] > NORTH_BORDER:  # ������-�������� ������� ������
                    print("North east border")
                    map = NORTH_east_border
                    image = 'north_east_border.jpg'
                else:
                    print("North east")
                    map = NORTH_east
                    image = 'north_east.jpg'
            elif coordinates[0] < BELOW_CENTER:  # ��� �������� �����
                print("South east")
                map = SOUTH_east
                image = 'south_east.jpg'
            else:
                print("right bank center")
                map = CENTER_right_city
                image = 'right_center.jpg'
    return [map, image]


def map_point(coordinates, district):
    coord_x = coordinates[1]
    coord_y = coordinates[0]
    x_min = district[0][0]
    x_max = district[0][1]
    y_min = district[1][0]
    y_max = district[1][1]
    pxx_min = 320
    pxx_max = 0
    pxy_min = 240
    pxy_max = 0
    coord_x_span = x_max - x_min
    coord_y_span = y_max - y_min
    x_span = pxx_max - pxx_min
    y_span = pxy_max - pxy_min

    valueScaled_x = float(coord_x - x_min) / float(coord_x_span)
    pxlxcord = pxx_min + (valueScaled_x * x_span)
    pxlxcord = int(pxlxcord)
    valueScaled_y = float(coord_y - y_min) / float(coord_y_span)
    pxlycord = pxy_min + (valueScaled_y * y_span)
    pxlycord = int(pxlycord)

    return [pxlycord, 320 - pxlxcord]


def pxl(dbm_input):
    module_min = -110
    module_max = -54
    pxl_min = 220
    pxl_max = 50
    module_value_span = module_max - module_min
    actual_dbm_span = pxl_max - pxl_min

    valueScaled = float(dbm_input - module_min) / float(module_value_span)
    signal_level = pxl_min + (valueScaled * actual_dbm_span)
    signal_level = int(signal_level)
    return signal_level


def toFixed(f, n=0):
    a, b = str(f).split('.')
    return '{}.{}{}'.format(a, b[:n], '0' * (n - len(b)))


def pointer(pin):
    if pin == DOWN or pin == UP:
        point = 1
    global CURRENT
    CURRENT += point
    CURRENT = CURRENT % 2


def test_graph(pin):
    global CURRENT_LEVEL
    if pin == RIGHT:
        led(RGB_led, False)
        led(BLUE, True)
        global CURRENT
        if CURRENT == 0:
            CURRENT_LEVEL = GRAPH_LEVEL
        else:
            if CURRENT_LEVEL == MAP_LEVEL:

                CURRENT_LEVEL = BASESTATIONS_LEVEL
            elif CURRENT_LEVEL == BASESTATIONS_LEVEL:

                CURRENT_LEVEL = TABLE_LEVEL
            else:
                CURRENT_LEVEL = MAP_LEVEL


def mainwindow(pin):
    # ��������� �����
    global CURRENT_LEVEL
    global CURRENT
    if pin == LEFT:
        if CURRENT_LEVEL == BASESTATIONS_LEVEL:
            CURRENT_LEVEL = MAP_LEVEL
        else:
            led(RGB_led, False)
            led(GREEN, True)
            CURRENT = 0
            CURRENT_LEVEL = MAIN_LEVEL


def draw_table(bs_info, page):
    crd = bs_info
    lenght = len(crd)
    image = Image.open('table.jpg')
    draw_img = ImageDraw.Draw(image)
    draw_img.text((13, 8),
                  ' |{lat}|{long}|{radius}|{operator}|{rxlev}|{arfcn}'.format(lat='Latitude',
                                                                              long='Longitude',
                                                                              radius='Radius',
                                                                              operator='Operator',
                                                                              rxlev='Rxlevel',
                                                                              arfcn='Arfcn', font=H3,
                                                                              fill="WHITE"))
    step = 0
    for bs in range(lenght):
        crd[bs][0] = toFixed(crd[bs][0], 5)
        crd[bs][1] = toFixed(crd[bs][1], 5)
        if crd[bs][2] < 1000:
            crd[bs][2] = toFixed(crd[bs][2], 2)
        n = bs + 1 + page
        if n < 10:
            n = str(n)
            n = ' ' + n + ' '
        else:
            n = str(n)
        draw_img.text((7, 18 + step), '{n}|{lat}| {long} |  {radius}  |{operator}|     {rxlev}     |{arfcn}'
                      .format(n=n, lat=crd[bs][0], long=crd[bs][1], radius=crd[bs][2],
                              operator=crd[bs][3], rxlev=crd[bs][4], arfcn=crd[bs][5]),
                      font=H3, fill="WHITE")  # signature !
        step += 10
    image = image.rotate(90, 0, 1).resize((240, 320))
    TFT.display(image)
    time.sleep(5)


def stability_check():
    global operators
    state = SIM800.firststart()
    if state == 1:
        led(RGB_led, False)
        led([GREEN], True)
        SIM800.echo('Module provides full oportunities')
        image = Image.open('bg_fitted.jpg')
        draw_img = ImageDraw.Draw(image)
        SIGNAL_LEVEL = str(SIM800.get_signal_level())
        new_location = SIM800.get_gsm_location()
        location = new_location
        location = [float('{:.2f}'.format(location[0])), float('{:.2f}'.format(location[1]))]
        draw_img.text((210, 80 + 40), "{lat}n {long}e ".format(lat=location[1], long=location[0]), font=H2,
                      fill="WHITE")
        draw_img.text((210, 80), str(SIGNAL_LEVEL) + ' dBm', font=H2, fill="WHITE")  # signature !

        image = image.rotate(90, 0, 1)
        TFT.display(image)
        operators = SIM800.get_available_operators()
        return MAIN_LEVEL
    elif state == 2:
        global Coordinates
        image = Image.open('bg_fitted.jpg')
        draw_img = ImageDraw.Draw(image)
        SIGNAL_LEVEL = str(SIM800.get_signal_level())
        draw_img.text((210, 80), str(SIGNAL_LEVEL) + ' dBm', font=H2, fill="WHITE")  # signature !
        image = image.rotate(90, 0, 1)
        TFT.display(image)
        led(RGB_led, False)
        led([RED, GREEN], True)
        SIM800.echo('Sorry, you can only use mainwindows')
        image = Image.open('bg_fitted.jpg')
        draw_img = ImageDraw.Draw(image)
        draw_img.text((210, 80), str(SIGNAL_LEVEL) + ' dBm', font=H2, fill="WHITE")  # signature !
        operators = SIM800.get_available_operators()
        for operator in range(len(operators)):
            draw_img.text((210, 79 + 80 + 15 * operator), operators[operator], font=H3, fill="WHITE")  # signature !
        draw_img.rectangle([290, 92 + 40 * CURRENT, 299, 83 + 40 * CURRENT], fill='WHITE')
        image = image.rotate(90, 0, 1)
        TFT.display(image)
        Coordinates = SIM800.get_bs_around()

        return MAIN_LEVEL
    else:
        led(RGB_led, False)
        led([RED], True)
        return ERROR_LEVEL


# GPIO.wait_for_edge(LEFT and RIGHT, GPIO.BOTH)
draw = TFT.draw()
MAIN_LEVEL = [0, 0]
GRAPH_LEVEL = [0, 1]
MAP_LEVEL = [1, 1]
BASESTATIONS_LEVEL = [1, 2]
TABLE_LEVEL = [1, 3]
ERROR_LEVEL = [3, 3]

CURRENT_LEVEL = MAIN_LEVEL
SIGNAL_LEVEL = 0
CURRENT = 0
BAUDRATE = 115200
location = []
operators = []
Coordinates = []
port = serial.Serial(port='/dev/ttyAMA0',
                     baudrate=BAUDRATE,
                     bytesize=8,
                     parity='N',
                     stopbits=1,
                     timeout=0.03,
                     )
try:
    GPIO.add_event_detect(UP, GPIO.RISING, callback=pointer, bouncetime=200)
    GPIO.add_event_detect(DOWN, GPIO.RISING, callback=pointer, bouncetime=200)
    GPIO.add_event_detect(RIGHT, GPIO.RISING, callback=test_graph, bouncetime=200)
    # GPIO.add_event_detect(LEFT, GPIO.RISING, bouncetime=50, callback=mainwindow)
    GPIO.add_event_detect(LEFT, GPIO.RISING)
    image = Image.open('hello.jpg')
    draw_img = ImageDraw.Draw(image)
    image = image.rotate(90, 0, 1)
    TFT.display(image)
    SIM800 = GSM_module(115200)
    CURRENT_LEVEL = stability_check()

    while True:
        if CURRENT_LEVEL == MAIN_LEVEL:

            image = Image.open('bg_fitted.jpg')
            draw_img = ImageDraw.Draw(image)
            SIM800.read()
            SIGNAL_LEVEL = str(SIM800.get_signal_level())
            try:
                new_location = SIM800.get_gsm_location()
                location = new_location
                location = [float('{:.2f}'.format(location[0])), float('{:.2f}'.format(location[1]))]
                draw_img.text((210, 80 + 40), "{lat}n {long}e ".format(lat=location[1], long=location[0]), font=H2,
                              fill="WHITE")
            except IndexError:
                try:
                    location = Coordinates[1]
                    location = [float('{:.2f}'.format(location[0])), float('{:.2f}'.format(location[1]))]
                    draw_img.text((210, 80 + 40), "{lat} {long}".format(lat=location[1], long=location[0]), font=H2,
                                  fill="WHITE")
                except:
                    new_location = "no data"
                    location = new_location
                    draw_img.text((210, 80 + 40), "{} ".format(location), font=H2,
                                  fill="WHITE")

            draw_img.text((210, 80), str(SIGNAL_LEVEL) + ' dBm', font=H2, fill="WHITE")  # signature !

            for operator in range(len(operators)):
                draw_img.text((210, 79 + 80 + 15 * operator), operators[operator], font=H3, fill="WHITE")  # signature !
            draw_img.rectangle([290, 92 + 40 * CURRENT, 299, 83 + 40 * CURRENT], fill='WHITE')
            image = image.rotate(90, 0, 1)
            TFT.display(image)
            print('MAIN IS RUNNING')

        elif CURRENT_LEVEL == GRAPH_LEVEL:
            # graph_window(9)

            resolution = 9
            begin = 280
            end = 40
            step = (begin - end) / (resolution)
            step = int(step)
            # times = [280, 220, 160, 100, 40]
            times = []
            for s in range(resolution + 1):
                times.append(begin - s * step)
            data = [0] * resolution
            image = Image.open('dbm_graph.jpg')
            image = image.rotate(90, 0, 1).resize((240, 320))
            draw_img = ImageDraw.Draw(image)
            data[-1] = SIM800.get_signal_level()
            data[-1] = pxl(data[-1])
            for t in range(resolution):
                if GPIO.event_detected(LEFT):
                    mainwindow(LEFT)
                    break
                a = time.time()

                data[t] = SIM800.get_signal_level()
                if data[t] == None:
                    data[t] = data[t - 1]
                data[t] = pxl(data[t])
                draw_img.line((data[t - 1], times[t], data[t], times[t + 1]), fill=(0, 30, 250), width=2)
                print('Time: {} seconds'.format(a - time.time()))
                TFT.display(image)
            print('SOME GRAPHICS')
        elif CURRENT_LEVEL == MAP_LEVEL:
            image = Image.open('big_map_spb.jpg')
            draw_img = ImageDraw.Draw(image)
            if Coordinates != []:
                print('Got locations, draw a map...')
                new_location = Coordinates[1]
            else:
                try:
                    new_location = SIM800.get_gsm_location()
                except IndexError:
                    Coordinates = SIM800.get_bs_around()
                    new_location = Coordinates[1]
            location = new_location
            cords = map_point(location, BIG_MAP)
            # draw_img.rectangle([cords[1], cords[0],cords[1]+5, cords[0]+5], fill='WHITE')
            draw_img.ellipse([cords[1] - 5, cords[0] - 5, cords[1] + 5, cords[0] + 5], fill='RED')
            image = image.rotate(90, 0, 1).resize((240, 320))
            TFT.display(image)
            print('LOOK AT THIS BS')
            time.sleep(1)
            Coordinates = SIM800.get_bs_around()
            if GPIO.event_detected(LEFT):
                mainwindow(LEFT)

        elif CURRENT_LEVEL == BASESTATIONS_LEVEL:
            if Coordinates != []:
                print('Got locations, draw a map...')
            else:
                Coordinates = SIM800.get_bs_around()
            bs_cords = Coordinates[0]
            try:
                new_location = SIM800.get_gsm_location()
            except IndexError:
                new_location = Coordinates[1]
            location = new_location
            district = map_choose(location)
            image = Image.open(district[1])
            map_bs = district[0]
            location = map_point(location, map_bs)
            for bs in bs_cords:
                cords = map_point([bs[0], bs[1]], map_bs)
                draw_img = ImageDraw.Draw(image)
                if bs[4] > 45:
                    color = 'GREEN'
                elif bs[4] > 40:
                    color = 'BLUE'
                else:
                    color = 'YELLOW'

                draw_img.ellipse([cords[1] - 1, cords[0] - 1, cords[1] + 1, cords[0] + 1], fill=color)
                draw_img.rectangle([location[1] - 3, location[0] - 3, location[1] + 3, location[0] + 3], fill='RED')

            image = image.rotate(90, 0, 1).resize((240, 320))
            TFT.display(image)
            time.sleep(1)
            Coordinates = SIM800.get_bs_around()
            print('LOOK AT THIS BS')
            if GPIO.event_detected(LEFT):
                mainwindow(LEFT)

        elif CURRENT_LEVEL == TABLE_LEVEL:
            cells = Coordinates[0]
            print('Lenght: ', len(cells))
            if len(cells) >= 21:
                first_part = cells[0:21]
                second_part = cells[21:len(cells)]
                draw_table(first_part, 0)
                time.sleep(5)
                draw_table(second_part, 21)
                time.sleep(5)
            else:
                draw_table(cells,0)
                time.sleep(10)
            print('THIS WOULD BE A MAP')
            if GPIO.event_detected(LEFT):
                mainwindow(LEFT)
        else:
            led(RGB_led, False)
            led(RED, True)
            image = Image.open('error.jpg')
            image = image.rotate(90, 0, 1).resize((240, 320))
            TFT.display(image)
            for i in range(5):
                led(RGB_led, False)
                led(RED, True)
                time.sleep(500)
            CURRENT_LEVEL = stability_check()


except KeyboardInterrupt:
    print("Exit pressed Ctrl+C")

finally:
    GPIO.cleanup()
    print("End of Program")
